package com.example.tutorez;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tutorez.BD.MyOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class frAsistentesTutoria extends Fragment {
    ListView lista;
    List<String> list = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_asistentes_tutoria, container, false);
        SharedPreferences preferences = getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
        String id_tut = preferences.getString("id_tutoria","");
        MyOpenHelper dbHelper = new MyOpenHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db != null ) {
            Cursor c = db.rawQuery("SELECT id, nombre_user FROM mis_tutorias where id_tutoria="+id_tut, null);
            if (c != null && c.getCount()>0) {
                c.moveToFirst();
                do {
                    String BD_nombre_user = c.getString(c.getColumnIndex("nombre_user"));
                    list.add(BD_nombre_user);
                }while (c.moveToNext());
            }else{
                Toast.makeText(getContext(), "EL HAY ASISTENTES A ESTA TUTORIA", Toast.LENGTH_LONG).show();
            }
            c.close();
            db.close();
        }

        lista = (ListView) view.findViewById(R.id.lista_tutoria);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, list);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String value=adapter.getItem(position);
                Toast.makeText(getContext(), "EL ESTUDIANTE "+value+ " ASISTIRA A LA TUTORIA", Toast.LENGTH_LONG).show();

            }
        });

        return view;

    }
}
