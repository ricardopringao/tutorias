package com.example.tutorez;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.example.tutorez.BD.MyOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class frTutoriasPublicadas extends Fragment {

    ListView lista;
    List<String> list;
    List<String> listID = new ArrayList<String>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_tutorias_publicadas, container, false);

        MyOpenHelper dbHelper = new MyOpenHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        list = new ArrayList<String>();
        if (db != null) {
            SharedPreferences preferences = getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
            String id_user = preferences.getString("id","");
            Cursor c = db.rawQuery("SELECT id, nombre_materia , hora_inicio, descripcion FROM tutoria where id_user="+id_user, null);
            if (c != null && c.getCount()>0) {
                c.moveToFirst();
                do {
                    String BD_id = c.getString(c.getColumnIndex("id"));
                    String BD_materia = c.getString(c.getColumnIndex("nombre_materia"));
                    String BD_hora = c.getString(c.getColumnIndex("hora_inicio"));
                    String BD_descripcion = c.getString(c.getColumnIndex("descripcion"));
                    list.add(BD_materia+" \nHora: "+ BD_hora + " \n"+BD_descripcion);
                    listID.add(BD_id);
                }while (c.moveToNext());
            }else{
                Toast.makeText(getContext(), "NO EXISTEN TUTORIAS PUBLICADAS", Toast.LENGTH_LONG).show();
            }
            c.close();
            db.close();
        }


        lista = (ListView) view.findViewById(R.id.lista_tutoria);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, list);
        lista.setAdapter(adapter);


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String value=adapter.getItem(position);
                String index = String.valueOf(adapter.getItemId(position));
                String date_id = listID.get(Integer.parseInt(index));
                SharedPreferences.Editor editor = getActivity().getSharedPreferences("usuario", MODE_PRIVATE).edit();
                editor.putString("id_tutoria", date_id);
                editor.commit();
                Fragment miFragment = null;
                miFragment = new frAsistentesTutoria();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_main, miFragment).addToBackStack(null).commit();
            }
        });



        return view;

    }

}
