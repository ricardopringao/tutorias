package com.example.tutorez;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.tutorez.Util.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    EditText usuario, password;
    TextView nuevo;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener listener;
    String rol;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = (Button) findViewById(R.id.btn_login);
        usuario = (EditText) findViewById(R.id.text_usuario);
        password = (EditText) findViewById(R.id.text_password);
        nuevo = (TextView) findViewById(R.id.tv_soy_nuevo);

        mAuth = FirebaseAuth.getInstance();

        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser()!=null){
                    Log.e("rol", "esta logueado");
                    consultarRol();
                }else {
                    Log.e("rol", "no esta logueado");
                }
            }
        };

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = usuario.getText().toString();
                String pass = password.getText().toString();

                mAuth.signInWithEmailAndPassword(user,pass)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            consultarRol();
                        }else {
                            Toast.makeText(getApplicationContext(), "No se ha podido iniciar sesión", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

        nuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(getApplicationContext(), MainRegistro.class);
                startActivity(home);
            }
        });

    }

    private void iniciarSesion() {
        if (rol.equals("E")){
            startActivity(new Intent(LoginActivity.this, EstudianteActivity.class));
            Toast.makeText(getApplicationContext(), "Bienvenido Estudiante", Toast.LENGTH_SHORT).show();
        }if (rol.equals("P")){
            startActivity(new Intent(LoginActivity.this, DocenteActivity.class));
            Toast.makeText(getApplicationContext(), "Bienvenido Docente", Toast.LENGTH_SHORT).show();
        }
    }

    private void consultarRol() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    String key = myRef.child(mAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)) {
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        rol = usuario_perfil.getRol();
                        Log.e("rol", rol);
                        iniciarSesion();
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(listener);
    }
}
