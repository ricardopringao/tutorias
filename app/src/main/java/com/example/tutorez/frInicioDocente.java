package com.example.tutorez;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutorez.BD.MyOpenHelper;

import static android.content.Context.MODE_PRIVATE;


public class frInicioDocente extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inicio_docente, container, false);

        TextView text = (TextView)view.findViewById(R.id.tv_bienvenido);
        SharedPreferences preferences = this.getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
        text.setText(preferences.getString("nombre",""));
        return view;
    }
}
