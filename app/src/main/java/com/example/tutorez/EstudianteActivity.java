package com.example.tutorez;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.TextView;

import com.example.tutorez.Util.Usuario;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EstudianteActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiante);


        Toolbar toolbar = findViewById(R.id.toolbar_estudiante);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout_estudiante);
        NavigationView navigationView = findViewById(R.id.nav_view_estudiante);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Fragment miFragment = new frInicioEstudiante();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main_estudiante, miFragment).addToBackStack(null).commit();

        cargarInformacionUsuario();

    }

    private void cargarInformacionUsuario() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {

                    TextView txtName = findViewById(R.id.texto_inicio);
                    TextView txtEmail = findViewById(R.id.text_correo);
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    String key = myRef.child(mAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)) {
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        txtName.setText(usuario_perfil.getNombre());
                        txtEmail.setText(usuario_perfil.getCorreo());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.estudiante, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings_estudiante) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
            Intent home = new Intent(getApplication(), MainActivity.class);
            startActivity(home);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Fragment miFragment = null;
        boolean fragmentoSeleccionado = true;

        if (id == R.id.inicio_estudiante ){

            miFragment = new frInicioEstudiante();
            fragmentoSeleccionado = true;

        }else if (id==R.id.perfil_estudiante){

            miFragment = new frMiPerfil();
            fragmentoSeleccionado = true;

        }else if (id==R.id.tutoria_publicada){

            miFragment = new frTutoriasPublicadasEstu();
            fragmentoSeleccionado = true;

        }else if (id==R.id.mis_tutorias){

            miFragment = new frMisTutoriasEstu();
            fragmentoSeleccionado = true;

        }


        if (fragmentoSeleccionado == true) {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main_estudiante, miFragment).addToBackStack(null).commit();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout_estudiante);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
