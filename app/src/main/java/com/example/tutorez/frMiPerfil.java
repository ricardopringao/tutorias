package com.example.tutorez;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tutorez.Util.Usuario;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.content.Context.MODE_PRIVATE;


public class frMiPerfil extends Fragment {

    TextView textnombre;
    TextView textcorreo;
    TextView textrol;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil_docente, container, false);

        textnombre = view.findViewById(R.id.tv_nombre_info);
        textcorreo = view.findViewById(R.id.tv_curso);
        textrol = view.findViewById(R.id.tv_materia);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    String key = myRef.child(mAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)) {
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        textnombre.setText("NOMBRE: " + usuario_perfil.getNombre());
                        textcorreo.setText("CORREO: " + usuario_perfil.getCorreo());

                        if (usuario_perfil.getRol().equals("E")){
                            textrol.setText("ROL: ESTUDIANTE");
                        }else if (usuario_perfil.getRol().equals("P")){
                            textrol.setText("ROL: DOCENTE");
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return view;
    }

}
