package com.example.tutorez;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.example.tutorez.R;


public class FullscreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences preferences = getSharedPreferences("usuario", MODE_PRIVATE);
                String id_id = preferences.getString("id",null);
                String id_nombre = preferences.getString("nombre",null);
                String id_rol = preferences.getString("rol",null);

                if (id_id !=null && id_nombre !=null){
                    if (id_rol.equals("E")){
                        Intent homeIntent = new Intent(getApplicationContext(), EstudianteActivity.class);
                        startActivity(homeIntent);
                        finish();
                    }else{
                        if (id_rol.equals("P")){
                            Intent homeIntent = new Intent(getApplicationContext(), DocenteActivity.class);
                            startActivity(homeIntent);
                            finish();
                        }
                    }
                }else{
                    Intent homeIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(homeIntent);
                    finish();
                }

            }
        },1500);

    }
}
