package com.example.tutorez;

import android.app.Activity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import android.widget.AdapterView.OnItemSelectedListener;

import com.example.tutorez.BD.MyOpenHelper;
import com.example.tutorez.Util.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainRegistro extends Activity implements OnItemSelectedListener {

    private FirebaseAuth mAuth;
    String rol = "E";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_registro);


        Button btnRegistro = (Button) findViewById(R.id.btn_registro);
        final EditText usuario = (EditText) findViewById(R.id.text_correo);
        final EditText password = (EditText) findViewById(R.id.text_password);
        final EditText nombre = (EditText) findViewById(R.id.text_nombre);

        Spinner spinner = (Spinner) findViewById(R.id.spinner_registro);
        spinner.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<String>();
        categories.add("ESTUDIANTE");
        categories.add("PROFESOR");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        mAuth = FirebaseAuth.getInstance();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Usuario");


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String user = usuario.getText().toString();
                String pass = password.getText().toString();
                final String nombr = nombre.getText().toString();
                if (user.isEmpty() || pass.isEmpty() || nombr.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Por favor complete los campos vacios", Toast.LENGTH_LONG).show();
                }else{
                    mAuth.createUserWithEmailAndPassword(user, pass).
                            addOnCompleteListener(MainRegistro.this ,new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Usuario usuario = new Usuario();
                                usuario.setNombre(nombr);
                                usuario.setCorreo(user);
                                usuario.setRol(rol);
                                myRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario);
                                Toast.makeText(getApplicationContext(), "Registro Exitoso", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MainRegistro.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(getApplicationContext(), "No se pudo completar el registro", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("ESTUDIANTE")){ rol = "E";}else{
            if (item.equals("PROFESOR")){ rol ="P";}
        }
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }


}
