package com.example.tutorez;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tutorez.BD.MyOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class frMisTutoriasEstu extends Fragment {
    ListView lista;
    List<String> list = new ArrayList<String>();
    List<String> listID = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_mis_tutorias_estu, container, false);

        lista = (ListView) view.findViewById(R.id.lista_tutoria);

        SharedPreferences preferences = getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
        String id_user = preferences.getString("id","");

        MyOpenHelper dbHelper = new MyOpenHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db != null) {
            Cursor c = db.rawQuery("SELECT id, descripcion FROM mis_tutorias where id_user="+id_user, null);
            Log.d("Resultado" , c.toString());
            if (c!=null && c.getCount()>0) {
                c.moveToFirst();
                do {
                    String BD_id = c.getString(c.getColumnIndex("id"));
                    String BD_descripcion = c.getString(c.getColumnIndex("descripcion"));
                    list.add(BD_descripcion);
                    listID.add(BD_id);
                }while (c.moveToNext());
            }else{
                Toast.makeText(getContext(), "No tienes tutorias registradas", Toast.LENGTH_LONG).show();
            }
            c.close();
            db.close();
        }


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, list);
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String value=adapter.getItem(position);
                Toast.makeText(getContext(), "DEBES ASISTIR A ESTA TUTORIA", Toast.LENGTH_LONG).show();
            }
        });

        return view;

    }

}
