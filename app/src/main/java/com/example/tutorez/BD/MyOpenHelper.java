package com.example.tutorez.BD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyOpenHelper extends SQLiteOpenHelper {

    private static final String TABLA_PERSONA = "CREATE TABLE persona(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "nombre TEXT, materia TEXT, correo TEXT, rol TEXT, usuario TEXT, contrasena TEXT)";

    private static final String TABLA_TUTORIA = "CREATE TABLE tutoria(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id_user INTEGER, nombre_user TEXT, nombre_materia TEXT, hora_inicio TEXT, descripcion TEXT)";

    private static final String TABLA_MIS_TUTORIAS = "CREATE TABLE mis_tutorias(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "id_user INTEGER, nombre_user TEXT, id_tutoria INTEGER, descripcion TEXT)";

    private static final String DB_NAME = "BD.sqlite";
    private static final int DB_VERSION = 1;

    public MyOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_PERSONA);
        db.execSQL(TABLA_TUTORIA);
        db.execSQL(TABLA_MIS_TUTORIAS);

        db.execSQL("INSERT INTO persona(nombre, materia, correo, rol, usuario, contrasena) " +
                "VALUES ('Jose Perez','PROGAMACION MOVIL','4','E','1','1')");
        db.execSQL("INSERT INTO persona(nombre, materia, correo, rol, usuario, contrasena) " +
                "VALUES ('Andres Perez','MATEMATICAS','5','E','2','2')");
        db.execSQL("INSERT INTO persona(nombre, materia, correo, rol, usuario, contrasena) " +
                "VALUES ('Pepe Perez','REDES','5','E','3','3')");
        db.execSQL("INSERT INTO persona(nombre, materia, correo, rol, usuario, contrasena) " +
                "VALUES ('Marce Perez','BASE DE DATOS','6','E','4','4')");
        db.execSQL("INSERT INTO persona(nombre, materia, correo, rol, usuario, contrasena) " +
                "VALUES ('Dario Perez','COMUNICACION','7','P','5','5')");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}