package com.example.tutorez;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;


public class frInicioEstudiante extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fr_inicio_estudiante, container, false);

        TextView text = (TextView)view.findViewById(R.id.tv_bienvenido);
        SharedPreferences preferences = this.getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
        text.setText(preferences.getString("nombre",""));
        return view;
    }
}
