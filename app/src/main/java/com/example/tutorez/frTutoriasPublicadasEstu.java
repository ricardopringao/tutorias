package com.example.tutorez;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tutorez.BD.MyOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class frTutoriasPublicadasEstu extends Fragment {

    ListView lista;
    List<String> list = new ArrayList<String>();
    List<String> listID = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_tutorias_publicadas_estu, container, false);

        MyOpenHelper dbHelper = new MyOpenHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (db != null) {
            Cursor c = db.rawQuery("SELECT id, nombre_materia, nombre_user , hora_inicio, descripcion FROM tutoria", null);
            if (c != null && c.getCount()>0) {
                c.moveToFirst();
                do {
                    String BD_id = c.getString(c.getColumnIndex("id"));
                    String BD_nombre = c.getString(c.getColumnIndex("nombre_user"));
                    String BD_materia = c.getString(c.getColumnIndex("nombre_materia"));
                    String BD_hora = c.getString(c.getColumnIndex("hora_inicio"));
                    String BD_descripcion = c.getString(c.getColumnIndex("descripcion"));
                    list.add(BD_materia+" \nProf. "+BD_nombre+" \nHora: "+ BD_hora + " \n"+BD_descripcion);
                    listID.add(BD_id);
                }while (c.moveToNext());
            }else{
                Toast.makeText(getContext(), "NO EXISTEN TUTORIAS PUBLICADAS", Toast.LENGTH_LONG).show();
            }
            c.close();
            db.close();
        }

        lista = (ListView) view.findViewById(R.id.lista_tutoria);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, list);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                String index = String.valueOf(adapter.getItemId(position));
                String value=adapter.getItem(position);
                String date_id = listID.get(Integer.parseInt(index));

                SharedPreferences preferences = getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
                String id_user = preferences.getString("id","");
                String nombre_user = preferences.getString("nombre","");

                if (validar(id_user, date_id) ==0){
                    MyOpenHelper dbHelper = new MyOpenHelper(getContext());
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    if (db != null) {
                        ContentValues cv = new ContentValues();
                        cv.put("id_user", id_user);
                        cv.put("nombre_user", nombre_user);
                        cv.put("id_tutoria", date_id);
                        cv.put("id_user", id_user);
                        cv.put("descripcion", value);
                        db.insert("mis_tutorias", null, cv);
                        Toast.makeText(getContext(), "ASISTENCIA A TUTORIA GUARDADA", Toast.LENGTH_LONG).show();
                    }
                    db.close();
                }else{
                    Toast.makeText(getContext(), "Ya estas registrado en esta tutoria", Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;

    }


    public int validar(String id_user, String id_tutoria){
        Integer value = 0;
        MyOpenHelper dbHelper = new MyOpenHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        if (db != null) {
            Cursor c = db.rawQuery("SELECT id_tutoria FROM mis_tutorias where id_user="+id_user, null);
            if (c!=null && c.getCount()>0) {
                c.moveToFirst();
                do {
                    String BD_id = c.getString(c.getColumnIndex("id_tutoria"));
                    if (BD_id.equals(id_tutoria)){
                        value = 1;
                    }
                }while (c.moveToNext());
            }
            c.close();
            db.close();
        }
        return value;

    }

}
