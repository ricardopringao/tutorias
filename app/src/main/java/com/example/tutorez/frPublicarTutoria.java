package com.example.tutorez;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutorez.BD.MyOpenHelper;

import static android.content.Context.MODE_PRIVATE;

public class frPublicarTutoria extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_publicar_tutoria, container, false);

        final EditText textmateria = (EditText)view.findViewById(R.id.text_materia);
        final EditText texthora = (EditText)view.findViewById(R.id.text_hora);
        final EditText textdescripcion = (EditText)view.findViewById(R.id.text_descripcion);
        Button btn_publicar = (Button) view.findViewById(R.id.tv_publicar);


        btn_publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textmateria.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "CAMPO MATERIA VACIO ", Toast.LENGTH_LONG).show();
                }else {
                    if (texthora.getText().toString().isEmpty()){
                        Toast.makeText(getContext(), "CAMPO HORA VACIO ", Toast.LENGTH_LONG).show();
                    }else{
                        if (textdescripcion.getText().toString().isEmpty()){
                            Toast.makeText(getContext(), "CAMPO DESCRIPCION VACIO ", Toast.LENGTH_LONG).show();
                        }else {
                            MyOpenHelper dbHelper = new MyOpenHelper(getContext());
                            SQLiteDatabase db = dbHelper.getWritableDatabase();
                            if (db != null) {
                                SharedPreferences preferences = getActivity().getSharedPreferences("usuario", MODE_PRIVATE);
                                String id_user = preferences.getString("id","");
                                String nombre_user = preferences.getString("nombre","");
                                ContentValues cv = new ContentValues();
                                cv.put("id_user", id_user);
                                cv.put("nombre_user", nombre_user);
                                cv.put("nombre_materia", textmateria.getText().toString());
                                cv.put("hora_inicio", texthora.getText().toString());
                                cv.put("descripcion", textdescripcion.getText().toString());
                                db.insert("tutoria", null, cv);
                                Toast.makeText(getContext(), "TUTORIA PUBLICADA CON EXITO", Toast.LENGTH_LONG).show();
                                textmateria.setText("");
                                textdescripcion.setText("");
                                texthora.setText("");
                            }
                        }
                    }
                }
            }
        });


        return view;
    }
}
