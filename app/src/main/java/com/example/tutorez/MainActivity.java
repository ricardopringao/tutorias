package com.example.tutorez;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences preferences = getSharedPreferences("usuario", MODE_PRIVATE);
                String id_id = preferences.getString("id",null);
                String id_nombre = preferences.getString("id",null);
                String id_rol = preferences.getString("id",null);

                if (id_id !=null && id_nombre !=null){
                    if (id_rol.equals("E")){

                        Intent homeIntent = new Intent(MainActivity.this, EstudianteActivity.class);
                        startActivity(homeIntent);
                        finish();
                    }else{
                        if (id_rol.equals("P")){
                            Intent homeIntent = new Intent(MainActivity.this, DocenteActivity.class);
                            startActivity(homeIntent);
                            finish();
                        }
                    }
                }else{
                    Intent homeIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(homeIntent);
                    finish();
                }

            }
        },1500);



    }
}




